# Monies, No Object

Alex scrunched the discarded plastiwrap tight around herself, patching gaps and chasing bubbles of chill air from her frozen body. The heating often failed in Central, but she wasn’t here by choice. The undesirable central decks were the only place to bed down without being hassled by those station security bullies. Alex feared them, and rightly so. They’d murdered her parents and repeatedly abused her. They stayed away from Central, mostly, too wintry for their bitter hearts perhaps?

She read the advertising on the plastiwrap, it was packaging for a disposable three piece business suit. She cried herself to sleep.