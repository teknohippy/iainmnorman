# Prayer to Self

The slow motion star-field rotates before me, framed by my misted remlok. My laboured breathing is deafening in the silence. I have run out of time.
I wait…
I always suspected it was complete bullshit, there’s no flashing before your eyes, no fast-forward replay of your life. Nothing.
So this is it. The End.
What a pointless waste of toil. Years of upholding the pretence of contentment, stuck in the same repeated patterns, striving for the perfect life.
Now I know the truth, you can’t get there, it’s not for sale. You lie to everyone, you lie to yourself.
I wish I’d let myself be happier.  