# Pilot's Federation Annual Dinner Dance

“May I have this dance?” Kenneth said, guiding his wife towards the dance floor.
She placed her arm around him, “A good year my love, you’ve finally fitted that engine.”
“We’d have never afforded it without that deal you made Sera.”
“Ah but we only succeeded because you out-flew those pirates.”
“I wasn’t the one manning the lasers.”
They smiled, spiralling around the dance floor. 

----
 
“Who’s that old man?” you ask, nodding towards a seated man, eyes closed, swaying.
“That’s Ken, came with his wife for years until she died in a cockpit fire. Now he just comes and watches.”