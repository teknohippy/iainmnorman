# Long Case

My Grandfather’s clock had always intrigued me. It was immaculate in every way considering it was a three hundred year old family heirloom. It's complex innards remained visible through a perfect cylinder of lead crystal with a hemispherical dome. The intricate, pristine brass gears and springs displayed a remarkable resistance to the passage of time. Yet I’d never seen the hands ever move. Grandfather said it stopped years ago.
“How long ago?” I'd once asked.
“Long before you were born,” he replied, “at least two hundred years.”
“Can't it be repaired?”
He paused and shifted his weight to the opposite corner of his armchair. “Well,” he said, glancing at the clock, “it is not exactly broken. I find it works fine.”
“Then why doesn't it move? Can we wind it up?”
“The... creator left instructions the glass must never be removed, for then it would truly be broken,” he stroked his beard, “You are very young still. When you are older, and it is your turn, I shall tell you all about that wonderful timepiece.”

----

In the years that followed my Grandfather ignored any further questions about the clock. Eventually, I gave up wondering, why bother worrying about a clock that doesn't tell the time? In fact I stopped thinking of it as a clock all together, it was just 'The Old Clock'.
In the summer of my fifteenth year, we were in the garden together. I was deadheading the roses, while Grandfather relaxed in the shade.
“Bring me my pipe would you please?” he asked.
“You're not so old you can't get it yourself.” I retorted with a smile.
“Aye, but I know you like to dote upon me.” 
He was right I'd do anything for him, after all he'd looked after me since I was a few weeks old. 
“Where did you leave it this time?” I asked.
“It's in the drawing room, on the mantel.” he said.
I opened the French windows and entered the drawing room. The pipe was there on the mantel, next to the old clock. I picked up the pipe and turned to leave. As I did the stem must have caught against a brass foot. I looked back when I felt the sharp tug and saw the old clock sliding across the polished wooden mantel. I watched paralysed as it teetered on the edge and fell. 
The paralysis evaporated. Desperately, I stretched out, reaching, grasping, hoping…
Somehow, I caught it, cradling it against my chest. It was unbroken! 
I set it back on the mantel carefully but something was wrong, the glass had shifted. I lifted it, hands shaking and set it precisely in place.
I picked up the pipe from where I'd dropped it and returned to the garden.
The garden was empty, where had he gone? I called out, but there was no answer, only a warm summer breeze blowing through the garden lifting clouds of fine dust from my Grandfather’s chair.
